/*
Logging package that adds log level to the standard log package.
*/
package log

import (
	"fmt"
	"io"
	"log"
)

// Log levels.
const (
	LogError = iota
	LogWarn
	LogInfo
	LogDebug
	LogMax
)

type Logger struct {
	*log.Logger
	logLevel uint
}

// LogLevelError is the error returned when an invalid log level is
// specified.
type ErrLogLevel struct {
	level uint
}

func (err *ErrLogLevel) Error() string {
	return fmt.Sprintf("Invalid log level %d", err.level)
}

func New(out io.Writer, prefix string, flag int, level uint) (*Logger, error) {
	var logger Logger

	logger.Logger = log.New(out, prefix, flag)
	err := logger.SetLogLevel(level)

	return &logger, err
}

func (logger *Logger) SetLogLevel(level uint) error {
	if level >= LogMax {
		return &ErrLogLevel{level}
	}
	logger.logLevel = level

	return nil
}

func (logger *Logger) Error(v ...interface{}) {
	if logger.logLevel >= LogError {
		log.Print(v...)
	}
}

func (logger *Logger) Errorf(format string, v ...interface{}) {
	if logger.logLevel >= LogError {
		log.Printf(format, v...)
	}
}

func (logger *Logger) Warn(v ...interface{}) {
	if logger.logLevel >= LogWarn {
		log.Print(v...)
	}
}

func (logger *Logger) Warnf(format string, v ...interface{}) {
	if logger.logLevel >= LogWarn {
		log.Printf(format, v...)
	}
}

func (logger *Logger) Info(v ...interface{}) {
	if logger.logLevel >= LogInfo {
		log.Print(v...)
	}
}

func (logger *Logger) Infof(format string, v ...interface{}) {
	if logger.logLevel >= LogInfo {
		log.Printf(format, v...)
	}
}

func (logger *Logger) Debug(v ...interface{}) {
	if logger.logLevel >= LogDebug {
		log.Print(v...)
	}
}

func (logger *Logger) Debugf(format string, v ...interface{}) {
	if logger.logLevel >= LogDebug {
		log.Printf(format, v...)
	}
}
